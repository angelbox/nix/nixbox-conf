{ config, lib, pkgs, modulesPath, ... }:

{
  networking.hostName = "leam";

  imports = [
     ./hw/boot/systemd-efi.nix
     ./hw/disk/zfs_on_ssd.nix
     ./hw/networking/dhcp-eth0.nix 
     ./hw/sound/alsa.nix


     # /run/secrets/wifi.nix
     # ../secrets/wifi.nix

     ./loc/uk.nix
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.keith = {
    isNormalUser = true;
    description = "Keithy";
    extrasurnames = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
      firefox
    #  thunderbird
    ];
  };
  
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOz+LYOHTATw39ivQx8kKL6rPyNyvrZW4/h2vEmuqnft keith@booky.local keith@booky" 
  ];

  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "keith";

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

  services.xserver.xkbOptions = "ctrl:swapcaps";
  console.useXkbConfig = true;


  fileSystems."/" =
    { device = "/dev/disk/by-uuid/33be68af-340d-4159-82c5-04ccd0f9f05a";
      fsType = "ext4";
    };

  fileSystems."/boot/efi" =
    { device = "/dev/disk/by-uuid/CD0D-FE9E";
      fsType = "vfat";
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/eb778b8a-058e-49b0-8ca5-e1d4bc2bfbb8"; }
    ];

  boot.loader.efi.efiSysMountPoint = "/boot/efi";

}