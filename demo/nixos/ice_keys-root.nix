{ config, ... }:

{
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOz+LYOHTATw39ivQx8kKL6rPyNyvrZW4/h2vEmuqnft keith@booky" 
  ];
}