{ config, ... }: 

{
  services.ddclient.enable = true;
  services.ddclient.username = "keithhodges";
  services.ddclient.passwordFile =  "/home/keith/.config/dyndns.pass";
  services.ddclient.domains = [ "honey.homeunix.com" ];
}
