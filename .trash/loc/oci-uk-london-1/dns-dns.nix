# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  dns.hosts = {
    "dns1" = {
      description = "OpenDNS DNS 1";
      comment = "OpenDNS DNS 1";
      ip4 = "208.67.222.222";
      domainName = "resolver1.opendns.com";
    };

    "dns2" = {
      description = "OpenDNS DNS 2";
      comment = "OpenDNS DNS 2";
      ip4 = "208.67.220.220";
      domainName = "resolver2.opendns.com";
    };
  };
}