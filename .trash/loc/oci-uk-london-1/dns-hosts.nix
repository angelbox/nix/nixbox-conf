{ config, pkgs, lib, ... }:

{
  dns.hosts = { 
  
    "gateway" = {
      description = "OCI Gateway";
      nickname = "gateway"; # dns1 
      ip4 = "10.0.0.1";
      hostName = "gateway";
    };

    "field1" = {
      description = "Field Server Prototype";
      ip4 = "10.0.0.213";
      domainName = "field.servebbs.com";
    };
  };
}