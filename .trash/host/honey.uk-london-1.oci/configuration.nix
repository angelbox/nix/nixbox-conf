{ pkgs, lib, ... }: 

{
  environment.variables.THIS = builtins.toString ./.; 
 
  imports = [
    /etc/nixos/hw/as/server
    /etc/nixos/locations/oci-uk-london-1
    /run/secrets/deploy
  ];
  
  services.openssh.ports = [ 222 ];

  services.ddclient.enable = true;
  services.ddclient.username = "keithhodges";
  services.ddclient.passwordFile =  "/home/keith/.config/dyndns.pass";
  services.ddclient.domains = [ "field.servebbs.com" ];

  #services.dnsmasq.enable = true;
  #services.dnsmasq.resolveLocalQueries = true;

}
