# box-server: a configurable starting point

{ config, lib, ... }:

  let 
    includeAll = import ./box/function/findAllNixFiles.nixf lib; 
  in 
{
  imports = [ ./configuration.box.nix ] ++ (includeAll ./base );  
}